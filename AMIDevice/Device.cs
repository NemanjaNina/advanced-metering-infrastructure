﻿using Common;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AMIDevice
{
    public class Device : IDevice
    {
        #region Polja
        private int deviceCode;
        private double timestamp;
        private bool upaljen;
        private List<Tuple<EMeasurementType, double>> listaPodataka;
        private IGenerator generator;
        private IAgregator agregator;
        #endregion

        #region Propertiji
        public int DeviceCode { get => deviceCode;}
        public double Timestamp { get => timestamp; }      
        public bool Upaljen { get => upaljen; set => upaljen = value; }
        public List<Tuple<EMeasurementType, double>> ListaPodataka { get => listaPodataka; }
        public IAgregator Agregator { get => agregator; set => agregator = value; }
        public IGenerator Generator { get => generator; set => generator = value; }
        #endregion

        public Device()
        {
            deviceCode = this.GetHashCode();
            generator = new Generator();
            listaPodataka = new List<Tuple<EMeasurementType, double>>(4)
            {
                new Tuple<EMeasurementType, double>(EMeasurementType.Napon,0),
                new Tuple<EMeasurementType, double>(EMeasurementType.Struja,0),
                new Tuple<EMeasurementType, double>(EMeasurementType.AktivnaSnaga,0),
                new Tuple<EMeasurementType, double>(EMeasurementType.ReaktivnaSnaga,0),
            };
            upaljen = false;          
            GenerisiPodatke();
        }

        public void GenerisiPodatke()
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime naseVreme = DateTime.Now;
            timestamp = (naseVreme.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;           
            
            listaPodataka[0] = new Tuple<EMeasurementType, double>(EMeasurementType.Napon, generator.GenerisiDouble(200,260));           
            listaPodataka[1] = new Tuple<EMeasurementType, double>(EMeasurementType.Struja, generator.GenerisiDouble(0,75));
            listaPodataka[2] = new Tuple<EMeasurementType, double>(EMeasurementType.AktivnaSnaga, listaPodataka[0].Item2 * listaPodataka[1].Item2);
            listaPodataka[3] = new Tuple<EMeasurementType, double>(EMeasurementType.ReaktivnaSnaga, generator.GenerisiDouble(0,20));
        }

        public string Upali()
        {
            string odgovor = "";
            if (Agregator == null)
            {
                odgovor += "Nije dodeljen nijedan Agregator!";
            }
            else
            {
                if (!upaljen)
                {
                    Upaljen = true;
                    Task.Factory.StartNew(() => PosaljiIzvestaj());
                    odgovor += String.Format("Uspesno upaljen device sa kodom: {0}!",DeviceCode);
                }
                else
                {
                    odgovor += "Device je vec upaljen!";
                }
            }
            return odgovor;
        }

        public string Ugasi()
        {
            string odgovor = "";
            if (Upaljen)
            {
                Upaljen = false;
                odgovor += String.Format("Uspesno ugasen device sa kodom: {0}",DeviceCode);
            }
            else
            {
                odgovor += "Device je vec ugasen!";
            }
            return odgovor;
        }

        public void PosaljiIzvestaj()
        {
            while (Upaljen)
            {
               
                try
                {
                    GenerisiPodatke();
                    Agregator.PrimiIzvestaj(this);
                }
                catch (Exception)
                {
                    
                }
                Thread.Sleep(1000);
            }
        }
    }
}
