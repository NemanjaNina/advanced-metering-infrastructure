﻿using Common;
using System;

namespace AMIDevice
{
    public class Generator : IGenerator
    {
        public double GenerisiDouble(double minimum,double maksimum)
        {
            return Math.Round(new Random().NextDouble() * (maksimum - minimum) + minimum, 2);      
        }

        
    }
}
