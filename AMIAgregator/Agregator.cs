﻿using Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace AMIAgregator
{
    public class Agregator : IAgregator
    {
        #region Polja
        private int agregatorCode;
        private double timestamp;
        private bool upaljen;
        private ISystemManagement systemManagement;
        private List<Tuple<Tuple<int,double>, List<Tuple<EMeasurementType, double>>>> listaPodataka;
        private PaketAgregatorSystem paketZaSlanje;
        #endregion

        #region Propertiji
        public int AgregatorCode { get => agregatorCode; }
        public double Timestamp { get => timestamp;}
        public bool Upaljen { get => upaljen; set => upaljen = value; }
        public List<Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>>> BufferBaza { get => listaPodataka; }
        public ISystemManagement SystemManagement { get => systemManagement; set => systemManagement = value; }
        #endregion

        public Agregator()
        {
            listaPodataka = new List<Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>>>();
            paketZaSlanje = new PaketAgregatorSystem();
            agregatorCode = GetHashCode();
            Upaljen = false;
        }

        public string Upali()
        {
            string odgovor = "";
            if (systemManagement == null)
            {
                odgovor += "Nije povezan na sistem!";
            }else if (Upaljen)
            {
                odgovor += "Agregator je vec upaljen!";
            }
            else
            {
                Upaljen = true;
                Task.Factory.StartNew(() => PosaljiIzvestaj());
                odgovor += "Agregator uspesno upaljen!";
            }

            return odgovor;
        }

        public string Ugasi()
        {
            string odgovor = "";
            if (Upaljen)
            {
                Upaljen = false;
                odgovor += "Agregator uspesno iskjucen!";
            }
            else
            {
                odgovor += "Agregator je vec iskjucen!";
            }

            return odgovor;
        }

        public void PrimiIzvestaj(IDevice device)
        {
            if (Upaljen)
            {                           
                List<Tuple<EMeasurementType, double>> listaDobijenihPodataka = new List<Tuple<EMeasurementType, double>>(4);
                listaDobijenihPodataka = device.ListaPodataka;
                Tuple<int, double> dobijenaTupla = new Tuple<int, double>(device.DeviceCode, device.Timestamp);
                listaPodataka.Add(new Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>>
                    (
                        dobijenaTupla,
                        listaDobijenihPodataka
                    ));
            }
        }

        public void PosaljiIzvestaj()
        {
            int period;
            if(Int32.TryParse(ConfigurationManager.AppSettings["PeriodSlanjaIzvestajaAgregatora"], out period))
            {
                period = period * 60000;
            }
            else
            {
                period = 300000;
            }
            while (Upaljen)
            {
                Thread.Sleep(period);
                PripremiPaket();
                if (systemManagement.PrimiIzvestaj(paketZaSlanje))
                {
                    listaPodataka.Clear();
                }

            }
        }

        private void PripremiPaket()
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime naseVreme = DateTime.Now;
            timestamp = (naseVreme.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            paketZaSlanje.AgregatorCode = AgregatorCode;
            paketZaSlanje.Timestamp = Timestamp;
            paketZaSlanje.ListaMerenja = listaPodataka;
        }
    }
}
