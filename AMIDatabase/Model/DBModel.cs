﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIDatabase.Model
{
    public class DBModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AgregatorCode { get; set; }
        public double AgregatorTimestamp { get; set; }
        public int DeviceCode { get; set; }
        public double DeviceTimestamp { get; set; }
        public double Napon { get; set; }
        public double Struja { get; set; }
        public double AktivnaSnaga { get; set; }
        public double ReaktivnaSnaga { get; set; }
        [NotMapped]
        public DateTime DatumAgregator
        {
            get
            {
                DateTime datum = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
                datum = datum.AddSeconds(AgregatorTimestamp).ToLocalTime();
                return datum;
            }
        }
        [NotMapped]
        public DateTime DatumDevice
        {
            get
            {
                DateTime datum = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
                datum = datum.AddSeconds(DeviceTimestamp).ToLocalTime();
                return datum;
            }
        }
        [NotMapped]
        public string Merenje { get; set; }
        [NotMapped]
        public double VrednostMerenja { get; set; }
    }
}
