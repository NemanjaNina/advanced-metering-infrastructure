﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIDatabase.Access
{
    public class DBManager
    {
        private static DBManager instance;

        private static double lowAlarmNapon = 207;
        private static double highAlarmNapon = 240;

        private static double lowAlarmStruja = 15;
        private static double highAlarmStruja = 75;

        private DBManager() { }

        public static DBManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DBManager();
                }

                return instance;
            }
        }

        public void AddDevice(Model.DBModel newModel)
        {
            using (var dbContext = new DeviceDBContext())
            {             
                Model.DBModel modelDB = dbContext.Merenja.Add(newModel);
                dbContext.SaveChanges();
            }
        }

        public List<Model.DBModel> VratiDeviceDatum(int deviceCode,double timeStart,double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();
            using (var dbContext = new DeviceDBContext())
            {
                foreach (var item in dbContext.Merenja)
                {
                    if (item.DeviceCode == deviceCode && item.DeviceTimestamp > timeStart && item.DeviceTimestamp < timeEnd)
                    {
                        result.Add(item);
                    }
                }
            }          
            return result;
        }

        public List<Model.DBModel> VratiProsekDeviceDatum(int deviceCode, double timeStart, double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();
            List<Model.DBModel> chartData = new List<Model.DBModel>();
            Model.DBModel temp = InicijalizujModel(0);
            int brojMerenja = 0;
            using (var dbContext = new DeviceDBContext())
            {
                foreach (var item in dbContext.Merenja)
                {
                    if (item.DeviceCode == deviceCode && item.DeviceTimestamp > timeStart && item.DeviceTimestamp < timeEnd)
                    {
                        result.Add(item);
                    }
                }
            }

            foreach (var item in result)
            {
                temp.Napon += item.Napon;
                temp.Struja += item.Struja;
                temp.AktivnaSnaga += item.AktivnaSnaga;
                temp.ReaktivnaSnaga += item.ReaktivnaSnaga;
                brojMerenja++;
            }

            var Napon = temp.Napon / brojMerenja;
            var Struja = temp.Struja / brojMerenja;
            var AktivnaSnaga = temp.AktivnaSnaga / brojMerenja;
            var ReaktivnaSnaga = temp.ReaktivnaSnaga / brojMerenja;

            temp.VrednostMerenja = Napon;
            temp.Merenje = "Napon";
            chartData.Add(temp);
            temp = InicijalizujModel(0);

            temp.VrednostMerenja = Struja;
            temp.Merenje = "Struja";
            chartData.Add(temp);
            temp = InicijalizujModel(0);

            temp.VrednostMerenja = AktivnaSnaga;
            temp.Merenje = "AktivnaSnaga";
            chartData.Add(temp);
            temp = InicijalizujModel(0);

            temp.VrednostMerenja = ReaktivnaSnaga;
            temp.Merenje = "ReaktivnaSnaga";
            chartData.Add(temp);

            

            return chartData;
        }


        
        private Model.DBModel InicijalizujModel(int agregatorCode)
        {
            Model.DBModel temp = new Model.DBModel();
            temp.AgregatorCode = agregatorCode;
            temp.AgregatorTimestamp = 0;
            temp.DeviceCode = 0;
            temp.DeviceTimestamp = 0;
            temp.Napon = 0;
            temp.Struja = 0;
            temp.AktivnaSnaga = 0;
            temp.ReaktivnaSnaga = 0;
            return temp;
        }

        public List<Model.DBModel> VratiSumuZaAgregatorIDatum(int agregatorCode, double timeStart, double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();
            List<Model.DBModel> chartResult = new List<Model.DBModel>();
            Model.DBModel temp = InicijalizujModel(agregatorCode);
            

            using (var dbContext = new DeviceDBContext())
            {
                //Izdvajanje merenja za zadati agregator i datum
                foreach (var item in dbContext.Merenja)
                {
                    if(item.AgregatorCode == agregatorCode && item.AgregatorTimestamp >= timeStart && item.AgregatorTimestamp <= timeEnd)
                    {
                        result.Add(item);
                    }
                }
            }

            //Grupisanje po timestampu
            var groupedByTime = result.GroupBy(x => x.AgregatorTimestamp);

            foreach (var item in groupedByTime)
            {
                //Izdvajanje merenja jednog timestampa u listu
                var listOfOneMoment = item.ToList();

                //Prikupljanje vrednosti sa svih merenja jednog timestampa
                int brojMerenja = 0;

                foreach (var item2 in listOfOneMoment)
                {
                    temp.Struja += item2.Struja;
                    temp.Napon += item2.Napon;
                    temp.AktivnaSnaga += item2.AktivnaSnaga;
                    temp.ReaktivnaSnaga += item2.ReaktivnaSnaga;
                    brojMerenja++;
                }
                temp.Napon = temp.Napon / brojMerenja;
                temp.AgregatorTimestamp = item.Key;
                chartResult.Add(temp);
                temp = InicijalizujModel(agregatorCode);
            }

            return chartResult;
        }

        public List<Model.DBModel> VratiProsekZaAgregatorIDatum(int agregatorCode, double timeStart, double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();
            List<Model.DBModel> chartResult = new List<Model.DBModel>();
            Model.DBModel temp = InicijalizujModel(agregatorCode);


            using (var dbContext = new DeviceDBContext())
            {
                //Izdvajanje merenja za zadati agregator i datum
                foreach (var item in dbContext.Merenja)
                {
                    if (item.AgregatorCode == agregatorCode && item.AgregatorTimestamp >= timeStart && item.AgregatorTimestamp <= timeEnd)
                    {
                        result.Add(item);
                    }
                }
            }

            //Grupisanje po timestampu
            var groupedByTime = result.GroupBy(x => x.AgregatorTimestamp);

            foreach (var item in groupedByTime)
            {
                //Izdvajanje merenja jednog timestampa u listu
                var listOfOneMoment = item.ToList();

                //Prikupljanje vrednosti sa svih merenja jednog timestampa
                int brojMerenja = 0;

                foreach (var item2 in listOfOneMoment)
                {
                    temp.Struja += item2.Struja;
                    temp.Napon += item2.Napon;
                    temp.AktivnaSnaga += item2.AktivnaSnaga;
                    temp.ReaktivnaSnaga += item2.ReaktivnaSnaga;
                    brojMerenja++;
                }
                temp.Napon = temp.Napon / brojMerenja;
                temp.Struja = temp.Struja / brojMerenja;
                temp.AktivnaSnaga = temp.AktivnaSnaga / brojMerenja;
                temp.ReaktivnaSnaga = temp.ReaktivnaSnaga / brojMerenja;
                temp.AgregatorTimestamp = item.Key;
                chartResult.Add(temp);
                temp = InicijalizujModel(agregatorCode);
            }

            return chartResult;
        }
        public List<Model.DBModel> AlarmNaponVisi(double timeStart, double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();                   
            using (var dbContext = new DeviceDBContext())
            {
                foreach (var item in dbContext.Merenja)
                {
                   if(item.Napon > highAlarmNapon && item.DeviceTimestamp >= timeStart && item.DeviceTimestamp <= timeEnd)
                   {
                        result.Add(item);
                   }
                }
            }             
            return result;
        }

        public List<Model.DBModel> AlarmNaponNizi(double timeStart, double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();
            using (var dbContext = new DeviceDBContext())
            {
                foreach (var item in dbContext.Merenja)
                {
                    if (item.Napon < lowAlarmNapon && item.DeviceTimestamp >= timeStart && item.DeviceTimestamp <= timeEnd)
                    {
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<Model.DBModel> AlarmStrujaVisi(double timeStart, double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();
            using (var dbContext = new DeviceDBContext())
            {
                foreach (var item in dbContext.Merenja)
                {
                    if (item.Struja > highAlarmStruja && item.DeviceTimestamp >= timeStart && item.DeviceTimestamp <= timeEnd)
                    {
                        result.Add(item);
                    }
                }
            }
            return result;
        }

        public List<Model.DBModel> AlarmStrujaNizi(double timeStart, double timeEnd)
        {
            List<Model.DBModel> result = new List<Model.DBModel>();
            using (var dbContext = new DeviceDBContext())
            {
                foreach (var item in dbContext.Merenja)
                {
                    if (item.Struja < lowAlarmStruja && item.DeviceTimestamp >= timeStart && item.DeviceTimestamp <= timeEnd)
                    {
                        result.Add(item);
                    }
                }
            }
            return result;
        }

    }
}
