﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIDatabase.Access
{
    class DeviceConfiguration : DbMigrationsConfiguration<DeviceDBContext>
    {
        public DeviceConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "DeviceDBContext";
        }
    }
}
