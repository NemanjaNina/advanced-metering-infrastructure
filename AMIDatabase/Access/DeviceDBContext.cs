﻿using System.Collections.Generic;
using System.Data.Entity;


namespace AMIDatabase.Access
{
    class DeviceDBContext : DbContext
    {
        public DeviceDBContext() : base("dbConnectionAgregator")
        {           
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<DeviceDBContext, DeviceConfiguration>());
        }
        public DbSet<Model.DBModel> Merenja { get; set; }


    }
}
