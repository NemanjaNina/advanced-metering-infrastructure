﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;
using AMIDevice;
using AMIAgregator;
using AMIDatabase.Access;
using AMIDatabase.Model;
using Infragistics.Win.DataVisualization;
using System.Threading;

namespace AMISystemManagement
{
    public partial class Form1 : Form
    {
        public static List<IAgregator> Agregatori = new List<IAgregator>();
        public static List<IDevice> Uredjaji = new List<IDevice>();

        public Form1()
        {
            InitializeComponent();  
            
        }

        private void DodajUredjaj_Click(object sender, EventArgs e)
        {
            Uredjaji.Add(new Device());
            PoveziListBoxSaDeviceListom();
        }
        private void buttonIzbrisiUredjaj_Click(object sender, EventArgs e)
        {
            IDevice selectedDevice = (Device)listBoxDevices.SelectedItem;
            if (selectedDevice == null)
            {
                MessageBox.Show("Nijedan uredjaj nije odabran");
            }
            else
            {
                selectedDevice.Ugasi();
                Uredjaji.Remove(selectedDevice);
                PoveziListBoxSaDeviceListom();
                MessageBox.Show("Uredjaj uklonjen");
            }
        }
        private void Reinicijalizuj(IDevice device)
        {
            device.Ugasi();
            Uredjaji.Remove(device);
            Uredjaji.Add(new Device());
            PoveziListBoxSaDeviceListom();
        }
        private void PoveziListBoxSaDeviceListom()
        {
            listBoxDevices.DataSource = null;
            listBoxDevices.DataSource = Uredjaji;
            listBoxDevices.DisplayMember = "DeviceCode";
            listBoxDevices.ValueMember = "DeviceCode";
        }
        private void PoveziListBoxSaAgregatorListom()
        {
            listBoxAgregators.DataSource = null;
            listBoxAgregators.DataSource = Agregatori;
            listBoxAgregators.DisplayMember = "AgregatorCode";
            listBoxAgregators.ValueMember = "AgregatorCode";
        }
        private void DodajAgregator_Click(object sender, EventArgs e)
        {
            Agregatori.Add(new Agregator());
            PoveziListBoxSaAgregatorListom();
        }
        private void buttonUpaliDevice_Click(object sender, EventArgs e)
        {
            IDevice selectedDevice = (Device)listBoxDevices.SelectedItem;
            if (selectedDevice == null)
            {
                MessageBox.Show("Nijedan uredjaj nije odabran");
            }
            else
            {
                MessageBox.Show(selectedDevice.Upali());
            }
        }
        private void buttonUgasiUredjaj_Click(object sender, EventArgs e)
        {
            IDevice selectedDevice = (Device)listBoxDevices.SelectedItem;
            if (selectedDevice == null)
            {
                MessageBox.Show("Nijedan uredjaj nije odabran");
            }
            else
            {
                MessageBox.Show(selectedDevice.Ugasi());
            }
        }
        private void buttonPrikaciNaAgregator_Click(object sender, EventArgs e)
        {
            IDevice selectedDevice = (Device)listBoxDevices.SelectedItem;
            IAgregator selectedAgregator = (Agregator)listBoxAgregators.SelectedItem;
            if (selectedDevice == null)
            {
                MessageBox.Show("Nijedan uredjaj nije odabran");
            }
            else if (selectedAgregator == null)
            {
                MessageBox.Show("Nijedan agregator nije odabran");
            }
            else if (selectedDevice.Agregator != null)
            {
                                
                    if (selectedDevice.Agregator.AgregatorCode == selectedAgregator.AgregatorCode)
                    {
                        MessageBox.Show("Ovaj uredjaj vec postoji u sistemu agregatora!Reinicijalizuje se...");
                        Reinicijalizuj(selectedDevice);
                    }           
                    else
                    {
                        if (selectedDevice.Upaljen)
                        {
                            selectedDevice.Ugasi();
                            selectedDevice.Agregator = selectedAgregator;
                            selectedDevice.Upali();
                        }
                        selectedDevice.Agregator = selectedAgregator;
                        MessageBox.Show("Uspesno prikljucen na agregator!");
                    }
            }
            else
            {
                selectedDevice.Agregator = selectedAgregator;
                MessageBox.Show("Uspesno prikljucen na agregator!");
            }

        }
        private void buttonIzbrisiAgregator_Click(object sender, EventArgs e)
        {
            IAgregator selectedAgregator = (Agregator)listBoxAgregators.SelectedItem;
            if (selectedAgregator == null)
            {
                MessageBox.Show("Nijedan agregator nije odabran");
            }
            else
            {
                selectedAgregator.Ugasi();
                Agregatori.Remove(selectedAgregator);
                PoveziListBoxSaAgregatorListom();
                MessageBox.Show("Agregator uklonjen");
            }
        }
        private void buttonUpaliAgregator_Click(object sender, EventArgs e)
        {
            IAgregator selectedAgregator = (Agregator)listBoxAgregators.SelectedItem;
            if (selectedAgregator == null)
            {
                MessageBox.Show("Nijedan agregator nije odabran");
            }
            else
            {
               MessageBox.Show(selectedAgregator.Upali());              
            }
        }
        private void buttonUgasiAgregator_Click(object sender, EventArgs e)
        {
            IAgregator selectedAgregator = (Agregator)listBoxAgregators.SelectedItem;
            if (selectedAgregator == null)
            {
                MessageBox.Show("Nijedan agregator nije odabran");
            }
            else
            {
                MessageBox.Show(selectedAgregator.Ugasi());
            }
        }
        private void buttonPrikaciNaSistem_Click(object sender, EventArgs e)
        {
            IAgregator selectedAgregator = (Agregator)listBoxAgregators.SelectedItem;
            if (selectedAgregator == null)
            {
                MessageBox.Show("Nijedan agregator nije odabran");
            }
            else
            {
                selectedAgregator.SystemManagement = new SystemManagement();
                MessageBox.Show("Uspesno prikacen na sistem");
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'systemDatabaseDataSet1.DBModels' table. You can move, or remove it, as needed.
            this.dBModelsTableAdapter1.Fill(this.systemDatabaseDataSet1.DBModels);
            // TODO: This line of code loads data into the 'systemDatabaseDataSet.DBModels' table. You can move, or remove it, as needed.
            this.dBModelsTableAdapter.Fill(this.systemDatabaseDataSet.DBModels);

        }


        private void FillChartFirst(string merenje,List<DBModel> baza,string datum)
        {
            var data = new ChartData(baza);

            DataChart1.IsHorizontalZoomEnabled = true;
            DataChart1.IsVerticalZoomEnabled = true;


            var xAxis = DataChart1.Axes.OfType<CategoryXAxis>().FirstOrDefault();
            xAxis.Label = datum;
            xAxis.DataSource = data;

            var series = DataChart1.Series.OfType<LineSeries>().FirstOrDefault();
            series.ValueMemberPath = merenje;
            series.DataSource = data;
            
        }
        private void FillChartThird(List<DBModel> baza)
        {
            var data = new ChartData(baza);

            DataChart3.IsHorizontalZoomEnabled = true;
            DataChart3.IsVerticalZoomEnabled = true;


            var xAxis = DataChart3.Axes.OfType<CategoryXAxis>().FirstOrDefault();
            xAxis.Label = "Merenje";
            xAxis.DataSource = data;

            var series = DataChart3.Series.OfType<LineSeries>().FirstOrDefault();
            series.ValueMemberPath = "VrednostMerenja";
            series.DataSource = data;

        }
        private void FillChartSecond(string merenje, List<DBModel> baza, string datum)
        {
            var data = new ChartData(baza);

            DataChart2.IsHorizontalZoomEnabled = true;
            DataChart2.IsVerticalZoomEnabled = true;


            var xAxis = DataChart2.Axes.OfType<CategoryXAxis>().FirstOrDefault();
            xAxis.Label = datum;
            xAxis.DataSource = data;

            var series = DataChart2.Series.OfType<LineSeries>().FirstOrDefault();
            series.ValueMemberPath = merenje;
            series.DataSource = data;

        }
        private void FillChartFourth(string merenje, List<DBModel> baza, string datum)
        {
            var data = new ChartData(baza);

            DataChart4.IsHorizontalZoomEnabled = true;
            DataChart4.IsVerticalZoomEnabled = true;


            var xAxis = DataChart4.Axes.OfType<CategoryXAxis>().FirstOrDefault();
            xAxis.Label = datum;
            xAxis.DataSource = data;

            var series = DataChart4.Series.OfType<LineSeries>().FirstOrDefault();
            series.ValueMemberPath = merenje;
            series.DataSource = data;

        }
        private void buttonIzvestaj_Click(object sender, EventArgs e)
        {          
            if (!Int32.TryParse(comboBoxUredjaj1.Text, out int deviceCode))
            {
                MessageBox.Show("Pogresan Device code!");
            }
            else
            {
                DateTime datum = dateTimePicker1.Value;
                DateTime pocetak = datum.Date;
                double timeStart = pocetak.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                DateTime kraj = pocetak.Add(new TimeSpan(23, 59, 59));
                double timeKraj = kraj.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                List<DBModel> result = DBManager.Instance.VratiDeviceDatum(deviceCode, timeStart, timeKraj);
                string merenje = comboBoxMerenje1.Text;
                FillChartFirst(merenje, result,"DatumDevice");              
            }            
        }
        private void buttonIzvestaj2_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(comboBoxAgregator1.Text, out int agregatorCode))
            {
                MessageBox.Show("Pogresan Agregtor code!");
            }
            else
            {
                string merenje = comboBoxMerenje2.Text;

                DateTime datum = dateTimePicker2.Value;
                DateTime pocetak = datum.Date;
                double timeStart = pocetak.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                DateTime kraj = pocetak.Add(new TimeSpan(23, 59, 59));
                double timeKraj = kraj.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

                List<DBModel> result = DBManager.Instance.VratiSumuZaAgregatorIDatum(agregatorCode, timeStart, timeKraj);
                FillChartSecond(merenje,result, "DatumAgregator");
            }
        }
        private void buttonIzvestaj3_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(comboBoxUredjaj3.Text, out int deviceCode))
            {
                MessageBox.Show("Pogresan Device code!");
            }
            else
            {
                DateTime datum = dateTimePicker3.Value;
                DateTime pocetak = datum.Date;
                double timeStart = pocetak.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                DateTime kraj = pocetak.Add(new TimeSpan(23, 59, 59));
                double timeKraj = kraj.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                List<DBModel> result = DBManager.Instance.VratiProsekDeviceDatum(deviceCode, timeStart, timeKraj);
                FillChartThird(result);
            }
        }
        private void buttonIzvestaj4_Click(object sender, EventArgs e)
        {
            if (!Int32.TryParse(comboBoxAgregator4.Text, out int agregatorCode))
            {
                MessageBox.Show("Pogresan Agregtor code!");
            }
            else
            {
                string merenje = comboBoxMerenje4.Text;

                DateTime datum = dateTimePicker4.Value;
                DateTime pocetak = datum.Date;
                double timeStart = pocetak.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                DateTime kraj = pocetak.Add(new TimeSpan(23, 59, 59));
                double timeKraj = kraj.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

                List<DBModel> result = DBManager.Instance.VratiProsekZaAgregatorIDatum(agregatorCode, timeStart, timeKraj);
                FillChartFourth(merenje, result, "DatumAgregator");
            }
        }
        private void buttonIzlistaj_Click(object sender, EventArgs e)
        {
            string operatorr = comboBoxOperator.Text;
            string merenje = comboBoxMerenje5.Text;

            DateTime datum1 = dateTimePicker5OD.Value;
            DateTime pocetni = datum1.Date;

            DateTime datum2 = dateTimePicker5DO.Value;
            DateTime krajnji = datum2.Date;
            krajnji = krajnji.Add(new TimeSpan(23, 59, 59));

            double datumOd = pocetni.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            double datumDo = krajnji.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

            List<DBModel> alarmnaLista = new List<DBModel>();
            switch (merenje)
            {
                case "Napon":
                    {
                        if (operatorr.Equals(">"))
                        {
                            alarmnaLista = DBManager.Instance.AlarmNaponVisi(datumOd, datumDo);
                        }
                        else
                        {
                            alarmnaLista = DBManager.Instance.AlarmNaponNizi(datumOd, datumDo);
                        }
                        break;
                    }
                case "Struja":
                    {
                        if (operatorr.Equals(">"))
                        {
                            alarmnaLista = DBManager.Instance.AlarmStrujaVisi(datumOd, datumDo);
                        }
                        else
                        {
                            alarmnaLista = DBManager.Instance.AlarmStrujaNizi(datumOd, datumDo);
                        }
                        break;
                    }            
                default:
                    {
                        MessageBox.Show("Pogresan parametar merenja!");
                        break;
                    }
            }

            var data = new DataGridSource(alarmnaLista);
            dataGridView1.DataSource = data;
           
        }
    }
}
