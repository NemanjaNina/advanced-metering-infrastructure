﻿namespace AMISystemManagement
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Infragistics.Win.DataVisualization.CategoryXAxis categoryXAxis5 = new Infragistics.Win.DataVisualization.CategoryXAxis();
            Infragistics.Win.DataVisualization.NumericYAxis numericYAxis5 = new Infragistics.Win.DataVisualization.NumericYAxis();
            Infragistics.Win.DataVisualization.LineSeries lineSeries5 = new Infragistics.Win.DataVisualization.LineSeries();
            Infragistics.Win.DataVisualization.CategoryXAxis categoryXAxis6 = new Infragistics.Win.DataVisualization.CategoryXAxis();
            Infragistics.Win.DataVisualization.NumericYAxis numericYAxis6 = new Infragistics.Win.DataVisualization.NumericYAxis();
            Infragistics.Win.DataVisualization.LineSeries lineSeries6 = new Infragistics.Win.DataVisualization.LineSeries();
            Infragistics.Win.DataVisualization.CategoryXAxis categoryXAxis7 = new Infragistics.Win.DataVisualization.CategoryXAxis();
            Infragistics.Win.DataVisualization.NumericYAxis numericYAxis7 = new Infragistics.Win.DataVisualization.NumericYAxis();
            Infragistics.Win.DataVisualization.LineSeries lineSeries7 = new Infragistics.Win.DataVisualization.LineSeries();
            Infragistics.Win.DataVisualization.CategoryXAxis categoryXAxis8 = new Infragistics.Win.DataVisualization.CategoryXAxis();
            Infragistics.Win.DataVisualization.NumericYAxis numericYAxis8 = new Infragistics.Win.DataVisualization.NumericYAxis();
            Infragistics.Win.DataVisualization.LineSeries lineSeries8 = new Infragistics.Win.DataVisualization.LineSeries();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonIzlistaj = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dateTimePicker5DO = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker5OD = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxMerenje5 = new System.Windows.Forms.ComboBox();
            this.comboBoxOperator = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.buttonIzvestaj4 = new System.Windows.Forms.Button();
            this.comboBoxAgregator4 = new System.Windows.Forms.ComboBox();
            this.dBModelsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.systemDatabaseDataSet1 = new AMISystemManagement.SystemDatabaseDataSet1();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxMerenje4 = new System.Windows.Forms.ComboBox();
            this.DataChart4 = new Infragistics.Win.DataVisualization.UltraDataChart();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.buttonIzvestaj3 = new System.Windows.Forms.Button();
            this.comboBoxUredjaj3 = new System.Windows.Forms.ComboBox();
            this.dBModelsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.systemDatabaseDataSet = new AMISystemManagement.SystemDatabaseDataSet();
            this.label12 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.DataChart3 = new Infragistics.Win.DataVisualization.UltraDataChart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonIzvestaj2 = new System.Windows.Forms.Button();
            this.comboBoxAgregator1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxMerenje2 = new System.Windows.Forms.ComboBox();
            this.DataChart2 = new Infragistics.Win.DataVisualization.UltraDataChart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonIzvestaj = new System.Windows.Forms.Button();
            this.comboBoxUredjaj1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxMerenje1 = new System.Windows.Forms.ComboBox();
            this.DataChart1 = new Infragistics.Win.DataVisualization.UltraDataChart();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonIzbrisiAgregator = new System.Windows.Forms.Button();
            this.buttonIzbrisiUredjaj = new System.Windows.Forms.Button();
            this.listBoxDevices = new System.Windows.Forms.ListBox();
            this.buttonPrikaciNaAgregator = new System.Windows.Forms.Button();
            this.buttonPrikaciNaSistem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.DodajAgregator = new System.Windows.Forms.Button();
            this.buttonUgasiAgregator = new System.Windows.Forms.Button();
            this.buttonUpaliAgregator = new System.Windows.Forms.Button();
            this.buttonUpaliDevice = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxAgregators = new System.Windows.Forms.ListBox();
            this.buttonUgasiUredjaj = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.dBModelsTableAdapter = new AMISystemManagement.SystemDatabaseDataSetTableAdapters.DBModelsTableAdapter();
            this.dBModelsTableAdapter1 = new AMISystemManagement.SystemDatabaseDataSet1TableAdapters.DBModelsTableAdapter();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dBModelsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemDatabaseDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart4)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dBModelsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemDatabaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart3)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.Black;
            this.tabPage6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage6.BackgroundImage")));
            this.tabPage6.Controls.Add(this.dataGridView1);
            this.tabPage6.Controls.Add(this.buttonIzlistaj);
            this.tabPage6.Controls.Add(this.label17);
            this.tabPage6.Controls.Add(this.label16);
            this.tabPage6.Controls.Add(this.dateTimePicker5DO);
            this.tabPage6.Controls.Add(this.dateTimePicker5OD);
            this.tabPage6.Controls.Add(this.label15);
            this.tabPage6.Controls.Add(this.label14);
            this.tabPage6.Controls.Add(this.comboBoxMerenje5);
            this.tabPage6.Controls.Add(this.comboBoxOperator);
            this.tabPage6.Font = new System.Drawing.Font("Yu Gothic UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage6.ForeColor = System.Drawing.Color.Red;
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1143, 594);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Alarmna stanja";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FloralWhite;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(25, 119);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1092, 379);
            this.dataGridView1.TabIndex = 9;
            // 
            // buttonIzlistaj
            // 
            this.buttonIzlistaj.BackColor = System.Drawing.Color.FloralWhite;
            this.buttonIzlistaj.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.buttonIzlistaj.FlatAppearance.BorderSize = 0;
            this.buttonIzlistaj.Font = new System.Drawing.Font("Lucida Console", 13F, System.Drawing.FontStyle.Bold);
            this.buttonIzlistaj.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonIzlistaj.Location = new System.Drawing.Point(360, 517);
            this.buttonIzlistaj.Name = "buttonIzlistaj";
            this.buttonIzlistaj.Size = new System.Drawing.Size(408, 40);
            this.buttonIzlistaj.TabIndex = 8;
            this.buttonIzlistaj.Text = "IZLISTAJ";
            this.buttonIzlistaj.UseVisualStyleBackColor = false;
            this.buttonIzlistaj.Click += new System.EventHandler(this.buttonIzlistaj_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label17.Location = new System.Drawing.Point(903, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 17);
            this.label17.TabIndex = 7;
            this.label17.Text = "DO";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label16.Location = new System.Drawing.Point(586, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 17);
            this.label16.TabIndex = 6;
            this.label16.Text = "OD";
            // 
            // dateTimePicker5DO
            // 
            this.dateTimePicker5DO.Location = new System.Drawing.Point(827, 47);
            this.dateTimePicker5DO.Name = "dateTimePicker5DO";
            this.dateTimePicker5DO.Size = new System.Drawing.Size(200, 30);
            this.dateTimePicker5DO.TabIndex = 5;
            // 
            // dateTimePicker5OD
            // 
            this.dateTimePicker5OD.Location = new System.Drawing.Point(509, 48);
            this.dateTimePicker5OD.Name = "dateTimePicker5OD";
            this.dateTimePicker5OD.Size = new System.Drawing.Size(200, 30);
            this.dateTimePicker5OD.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label15.Location = new System.Drawing.Point(318, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "Operator";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label14.Location = new System.Drawing.Point(137, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "Merenje";
            // 
            // comboBoxMerenje5
            // 
            this.comboBoxMerenje5.BackColor = System.Drawing.Color.FloralWhite;
            this.comboBoxMerenje5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMerenje5.FormattingEnabled = true;
            this.comboBoxMerenje5.Items.AddRange(new object[] {
            "Napon",
            "Struja"});
            this.comboBoxMerenje5.Location = new System.Drawing.Point(116, 51);
            this.comboBoxMerenje5.Name = "comboBoxMerenje5";
            this.comboBoxMerenje5.Size = new System.Drawing.Size(123, 31);
            this.comboBoxMerenje5.TabIndex = 1;
            // 
            // comboBoxOperator
            // 
            this.comboBoxOperator.BackColor = System.Drawing.Color.FloralWhite;
            this.comboBoxOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOperator.FormattingEnabled = true;
            this.comboBoxOperator.Items.AddRange(new object[] {
            ">",
            "<"});
            this.comboBoxOperator.Location = new System.Drawing.Point(305, 50);
            this.comboBoxOperator.Name = "comboBoxOperator";
            this.comboBoxOperator.Size = new System.Drawing.Size(123, 31);
            this.comboBoxOperator.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage5.BackgroundImage")));
            this.tabPage5.Controls.Add(this.buttonIzvestaj4);
            this.tabPage5.Controls.Add(this.comboBoxAgregator4);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.dateTimePicker4);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.comboBoxMerenje4);
            this.tabPage5.Controls.Add(this.DataChart4);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1143, 594);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Prosecna merenja za agregator";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // buttonIzvestaj4
            // 
            this.buttonIzvestaj4.Font = new System.Drawing.Font("Lucida Console", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIzvestaj4.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonIzvestaj4.Location = new System.Drawing.Point(369, 529);
            this.buttonIzvestaj4.Name = "buttonIzvestaj4";
            this.buttonIzvestaj4.Size = new System.Drawing.Size(408, 40);
            this.buttonIzvestaj4.TabIndex = 23;
            this.buttonIzvestaj4.Text = "IZVESTAJ";
            this.buttonIzvestaj4.UseVisualStyleBackColor = true;
            this.buttonIzvestaj4.Click += new System.EventHandler(this.buttonIzvestaj4_Click);
            // 
            // comboBoxAgregator4
            // 
            this.comboBoxAgregator4.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dBModelsBindingSource1, "AgregatorCode", true));
            this.comboBoxAgregator4.DataSource = this.dBModelsBindingSource1;
            this.comboBoxAgregator4.DisplayMember = "AgregatorCode";
            this.comboBoxAgregator4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAgregator4.FormattingEnabled = true;
            this.comboBoxAgregator4.Location = new System.Drawing.Point(385, 42);
            this.comboBoxAgregator4.Name = "comboBoxAgregator4";
            this.comboBoxAgregator4.Size = new System.Drawing.Size(160, 24);
            this.comboBoxAgregator4.TabIndex = 22;
            this.comboBoxAgregator4.ValueMember = "AgregatorCode";
            // 
            // dBModelsBindingSource1
            // 
            this.dBModelsBindingSource1.DataMember = "DBModels";
            this.dBModelsBindingSource1.DataSource = this.systemDatabaseDataSet1;
            // 
            // systemDatabaseDataSet1
            // 
            this.systemDatabaseDataSet1.DataSetName = "SystemDatabaseDataSet1";
            this.systemDatabaseDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label9.Location = new System.Drawing.Point(734, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Datum";
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(659, 40);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(221, 22);
            this.dateTimePicker4.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label10.Location = new System.Drawing.Point(414, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 17);
            this.label10.TabIndex = 19;
            this.label10.Text = "Agregator";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label11.Location = new System.Drawing.Point(149, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 17);
            this.label11.TabIndex = 18;
            this.label11.Text = "Merenje";
            // 
            // comboBoxMerenje4
            // 
            this.comboBoxMerenje4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMerenje4.FormattingEnabled = true;
            this.comboBoxMerenje4.Items.AddRange(new object[] {
            "Napon",
            "Struja",
            "AktivnaSnaga",
            "ReaktivnaSnaga"});
            this.comboBoxMerenje4.Location = new System.Drawing.Point(121, 42);
            this.comboBoxMerenje4.Name = "comboBoxMerenje4";
            this.comboBoxMerenje4.Size = new System.Drawing.Size(146, 24);
            this.comboBoxMerenje4.TabIndex = 17;
            // 
            // DataChart4
            // 
            categoryXAxis5.Id = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            categoryXAxis5.LabelMargin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            categoryXAxis5.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            numericYAxis5.Id = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            numericYAxis5.LabelHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Right;
            numericYAxis5.LabelLocation = Infragistics.Win.DataVisualization.AxisLabelsLocation.OutsideLeft;
            numericYAxis5.LabelMargin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            numericYAxis5.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            this.DataChart4.Axes.Add(categoryXAxis5);
            this.DataChart4.Axes.Add(numericYAxis5);
            this.DataChart4.BackColor = System.Drawing.Color.White;
            this.DataChart4.CrosshairPoint = new Infragistics.Win.DataVisualization.Point(double.NaN, double.NaN);
            this.DataChart4.Location = new System.Drawing.Point(36, 104);
            this.DataChart4.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart4.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart4.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart4.Name = "DataChart4";
            this.DataChart4.PreviewRect = new Infragistics.Win.DataVisualization.Rectangle(double.PositiveInfinity, double.PositiveInfinity, double.NegativeInfinity, double.NegativeInfinity);
            lineSeries5.AreaFillOpacity = double.NaN;
            lineSeries5.Id = new System.Guid("11c5a2d5-0a5f-4770-9edd-e0a2d24afc37");
            lineSeries5.MarkerCollisionAvoidance = Infragistics.Win.DataVisualization.CategorySeriesMarkerCollisionAvoidance.Omit;
            lineSeries5.MouseOverEnabled = true;
            lineSeries5.Title = "Series Title";
            lineSeries5.XAxisId = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            lineSeries5.YAxisId = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            this.DataChart4.Series.Add(lineSeries5);
            this.DataChart4.Size = new System.Drawing.Size(1074, 398);
            this.DataChart4.TabIndex = 16;
            this.DataChart4.Text = "ultraDataChart1";
            // 
            // tabPage4
            // 
            this.tabPage4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage4.BackgroundImage")));
            this.tabPage4.Controls.Add(this.buttonIzvestaj3);
            this.tabPage4.Controls.Add(this.comboBoxUredjaj3);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.dateTimePicker3);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.DataChart3);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1143, 594);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Prosecna merenja za uredjaj";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // buttonIzvestaj3
            // 
            this.buttonIzvestaj3.Font = new System.Drawing.Font("Lucida Console", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIzvestaj3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonIzvestaj3.Location = new System.Drawing.Point(369, 529);
            this.buttonIzvestaj3.Name = "buttonIzvestaj3";
            this.buttonIzvestaj3.Size = new System.Drawing.Size(408, 40);
            this.buttonIzvestaj3.TabIndex = 15;
            this.buttonIzvestaj3.Text = "IZVESTAJ";
            this.buttonIzvestaj3.UseVisualStyleBackColor = true;
            this.buttonIzvestaj3.Click += new System.EventHandler(this.buttonIzvestaj3_Click);
            // 
            // comboBoxUredjaj3
            // 
            this.comboBoxUredjaj3.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dBModelsBindingSource, "DeviceCode", true));
            this.comboBoxUredjaj3.DataSource = this.dBModelsBindingSource;
            this.comboBoxUredjaj3.DisplayMember = "DeviceCode";
            this.comboBoxUredjaj3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUredjaj3.FormattingEnabled = true;
            this.comboBoxUredjaj3.Location = new System.Drawing.Point(267, 46);
            this.comboBoxUredjaj3.Name = "comboBoxUredjaj3";
            this.comboBoxUredjaj3.Size = new System.Drawing.Size(160, 24);
            this.comboBoxUredjaj3.TabIndex = 14;
            this.comboBoxUredjaj3.ValueMember = "DeviceCode";
            // 
            // dBModelsBindingSource
            // 
            this.dBModelsBindingSource.DataMember = "DBModels";
            this.dBModelsBindingSource.DataSource = this.systemDatabaseDataSet;
            // 
            // systemDatabaseDataSet
            // 
            this.systemDatabaseDataSet.DataSetName = "SystemDatabaseDataSet";
            this.systemDatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label12.Location = new System.Drawing.Point(573, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 17);
            this.label12.TabIndex = 13;
            this.label12.Text = "Datum";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(498, 46);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(221, 22);
            this.dateTimePicker3.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label13.Location = new System.Drawing.Point(306, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 17);
            this.label13.TabIndex = 11;
            this.label13.Text = "Uredjaj";
            // 
            // DataChart3
            // 
            categoryXAxis6.Id = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            categoryXAxis6.LabelMargin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            categoryXAxis6.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            numericYAxis6.Id = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            numericYAxis6.LabelHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Right;
            numericYAxis6.LabelLocation = Infragistics.Win.DataVisualization.AxisLabelsLocation.OutsideLeft;
            numericYAxis6.LabelMargin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            numericYAxis6.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            this.DataChart3.Axes.Add(categoryXAxis6);
            this.DataChart3.Axes.Add(numericYAxis6);
            this.DataChart3.BackColor = System.Drawing.Color.White;
            this.DataChart3.CrosshairPoint = new Infragistics.Win.DataVisualization.Point(double.NaN, double.NaN);
            this.DataChart3.Location = new System.Drawing.Point(36, 104);
            this.DataChart3.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart3.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart3.Name = "DataChart3";
            this.DataChart3.PreviewRect = new Infragistics.Win.DataVisualization.Rectangle(double.PositiveInfinity, double.PositiveInfinity, double.NegativeInfinity, double.NegativeInfinity);
            lineSeries6.AreaFillOpacity = double.NaN;
            lineSeries6.Id = new System.Guid("ef82c28b-ee32-427b-a93a-d1c235853ddc");
            lineSeries6.MarkerCollisionAvoidance = Infragistics.Win.DataVisualization.CategorySeriesMarkerCollisionAvoidance.Omit;
            lineSeries6.MouseOverEnabled = true;
            lineSeries6.Title = "Series Title";
            lineSeries6.XAxisId = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            lineSeries6.YAxisId = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            this.DataChart3.Series.Add(lineSeries6);
            this.DataChart3.Size = new System.Drawing.Size(1060, 408);
            this.DataChart3.TabIndex = 8;
            this.DataChart3.Text = "ultraDataChart1";
            // 
            // tabPage3
            // 
            this.tabPage3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage3.BackgroundImage")));
            this.tabPage3.Controls.Add(this.buttonIzvestaj2);
            this.tabPage3.Controls.Add(this.comboBoxAgregator1);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.dateTimePicker2);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.comboBoxMerenje2);
            this.tabPage3.Controls.Add(this.DataChart2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1143, 594);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Suma merenja za Agregator";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // buttonIzvestaj2
            // 
            this.buttonIzvestaj2.Font = new System.Drawing.Font("Lucida Console", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIzvestaj2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonIzvestaj2.Location = new System.Drawing.Point(369, 529);
            this.buttonIzvestaj2.Name = "buttonIzvestaj2";
            this.buttonIzvestaj2.Size = new System.Drawing.Size(408, 40);
            this.buttonIzvestaj2.TabIndex = 15;
            this.buttonIzvestaj2.Text = "IZVESTAJ";
            this.buttonIzvestaj2.UseVisualStyleBackColor = true;
            this.buttonIzvestaj2.Click += new System.EventHandler(this.buttonIzvestaj2_Click);
            // 
            // comboBoxAgregator1
            // 
            this.comboBoxAgregator1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dBModelsBindingSource1, "AgregatorCode", true));
            this.comboBoxAgregator1.DataSource = this.dBModelsBindingSource1;
            this.comboBoxAgregator1.DisplayMember = "AgregatorCode";
            this.comboBoxAgregator1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAgregator1.FormattingEnabled = true;
            this.comboBoxAgregator1.Location = new System.Drawing.Point(419, 46);
            this.comboBoxAgregator1.Name = "comboBoxAgregator1";
            this.comboBoxAgregator1.Size = new System.Drawing.Size(160, 24);
            this.comboBoxAgregator1.TabIndex = 14;
            this.comboBoxAgregator1.ValueMember = "AgregatorCode";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label6.Location = new System.Drawing.Point(804, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Datum";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(726, 44);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(221, 22);
            this.dateTimePicker2.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label7.Location = new System.Drawing.Point(444, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Agregator";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label8.Location = new System.Drawing.Point(179, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "Merenje";
            // 
            // comboBoxMerenje2
            // 
            this.comboBoxMerenje2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMerenje2.FormattingEnabled = true;
            this.comboBoxMerenje2.Items.AddRange(new object[] {
            "Napon",
            "Struja",
            "AktivnaSnaga",
            "ReaktivnaSnaga"});
            this.comboBoxMerenje2.Location = new System.Drawing.Point(145, 46);
            this.comboBoxMerenje2.Name = "comboBoxMerenje2";
            this.comboBoxMerenje2.Size = new System.Drawing.Size(146, 24);
            this.comboBoxMerenje2.TabIndex = 9;
            // 
            // DataChart2
            // 
            categoryXAxis7.Id = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            categoryXAxis7.LabelMargin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            categoryXAxis7.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            numericYAxis7.Id = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            numericYAxis7.LabelHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Right;
            numericYAxis7.LabelLocation = Infragistics.Win.DataVisualization.AxisLabelsLocation.OutsideLeft;
            numericYAxis7.LabelMargin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            numericYAxis7.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            this.DataChart2.Axes.Add(categoryXAxis7);
            this.DataChart2.Axes.Add(numericYAxis7);
            this.DataChart2.BackColor = System.Drawing.Color.White;
            this.DataChart2.CrosshairPoint = new Infragistics.Win.DataVisualization.Point(double.NaN, double.NaN);
            this.DataChart2.Location = new System.Drawing.Point(37, 111);
            this.DataChart2.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart2.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart2.Name = "DataChart2";
            this.DataChart2.PreviewRect = new Infragistics.Win.DataVisualization.Rectangle(double.PositiveInfinity, double.PositiveInfinity, double.NegativeInfinity, double.NegativeInfinity);
            lineSeries7.AreaFillOpacity = double.NaN;
            lineSeries7.Id = new System.Guid("11c5a2d5-0a5f-4770-9edd-e0a2d24afc37");
            lineSeries7.MarkerCollisionAvoidance = Infragistics.Win.DataVisualization.CategorySeriesMarkerCollisionAvoidance.Omit;
            lineSeries7.MouseOverEnabled = true;
            lineSeries7.Title = "Series Title";
            lineSeries7.XAxisId = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            lineSeries7.YAxisId = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            this.DataChart2.Series.Add(lineSeries7);
            this.DataChart2.Size = new System.Drawing.Size(1060, 408);
            this.DataChart2.TabIndex = 8;
            this.DataChart2.Text = "ultraDataChart1";
            // 
            // tabPage2
            // 
            this.tabPage2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage2.BackgroundImage")));
            this.tabPage2.Controls.Add(this.buttonIzvestaj);
            this.tabPage2.Controls.Add(this.comboBoxUredjaj1);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.dateTimePicker1);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.comboBoxMerenje1);
            this.tabPage2.Controls.Add(this.DataChart1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1143, 594);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Merenja za izabrani uredjaj";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonIzvestaj
            // 
            this.buttonIzvestaj.Font = new System.Drawing.Font("Lucida Console", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIzvestaj.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonIzvestaj.Location = new System.Drawing.Point(369, 529);
            this.buttonIzvestaj.Name = "buttonIzvestaj";
            this.buttonIzvestaj.Size = new System.Drawing.Size(408, 40);
            this.buttonIzvestaj.TabIndex = 7;
            this.buttonIzvestaj.Text = "IZVESTAJ";
            this.buttonIzvestaj.UseVisualStyleBackColor = true;
            this.buttonIzvestaj.Click += new System.EventHandler(this.buttonIzvestaj_Click);
            // 
            // comboBoxUredjaj1
            // 
            this.comboBoxUredjaj1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dBModelsBindingSource, "DeviceCode", true));
            this.comboBoxUredjaj1.DataSource = this.dBModelsBindingSource;
            this.comboBoxUredjaj1.DisplayMember = "DeviceCode";
            this.comboBoxUredjaj1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUredjaj1.FormattingEnabled = true;
            this.comboBoxUredjaj1.Location = new System.Drawing.Point(456, 48);
            this.comboBoxUredjaj1.Name = "comboBoxUredjaj1";
            this.comboBoxUredjaj1.Size = new System.Drawing.Size(160, 24);
            this.comboBoxUredjaj1.TabIndex = 6;
            this.comboBoxUredjaj1.ValueMember = "DeviceCode";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label5.Location = new System.Drawing.Point(804, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Datum";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(732, 46);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(221, 22);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label4.Location = new System.Drawing.Point(501, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Uredjaj";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label3.Location = new System.Drawing.Point(187, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Merenje";
            // 
            // comboBoxMerenje1
            // 
            this.comboBoxMerenje1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMerenje1.FormattingEnabled = true;
            this.comboBoxMerenje1.Items.AddRange(new object[] {
            "Napon",
            "Struja",
            "AktivnaSnaga",
            "ReaktivnaSnaga"});
            this.comboBoxMerenje1.Location = new System.Drawing.Point(158, 48);
            this.comboBoxMerenje1.Name = "comboBoxMerenje1";
            this.comboBoxMerenje1.Size = new System.Drawing.Size(146, 24);
            this.comboBoxMerenje1.TabIndex = 1;
            // 
            // DataChart1
            // 
            categoryXAxis8.Id = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            categoryXAxis8.LabelMargin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            categoryXAxis8.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            numericYAxis8.Id = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            numericYAxis8.LabelHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Right;
            numericYAxis8.LabelLocation = Infragistics.Win.DataVisualization.AxisLabelsLocation.OutsideLeft;
            numericYAxis8.LabelMargin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            numericYAxis8.TitleHorizontalAlignment = Infragistics.Portable.Components.UI.HorizontalAlignment.Center;
            this.DataChart1.Axes.Add(categoryXAxis8);
            this.DataChart1.Axes.Add(numericYAxis8);
            this.DataChart1.BackColor = System.Drawing.Color.White;
            this.DataChart1.CrosshairPoint = new Infragistics.Win.DataVisualization.Point(double.NaN, double.NaN);
            this.DataChart1.Location = new System.Drawing.Point(37, 111);
            this.DataChart1.MarkerBrushes.Add(new Infragistics.Win.DataVisualization.SolidColorBrush(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))))));
            this.DataChart1.Name = "DataChart1";
            this.DataChart1.PreviewRect = new Infragistics.Win.DataVisualization.Rectangle(double.PositiveInfinity, double.PositiveInfinity, double.NegativeInfinity, double.NegativeInfinity);
            lineSeries8.AreaFillOpacity = double.NaN;
            lineSeries8.Id = new System.Guid("11c5a2d5-0a5f-4770-9edd-e0a2d24afc37");
            lineSeries8.MarkerCollisionAvoidance = Infragistics.Win.DataVisualization.CategorySeriesMarkerCollisionAvoidance.Omit;
            lineSeries8.MouseOverEnabled = true;
            lineSeries8.Title = "Series Title";
            lineSeries8.XAxisId = new System.Guid("150de178-3164-424e-bf8f-491930a7d2f3");
            lineSeries8.YAxisId = new System.Guid("1e52a6ae-2a6c-4862-808a-31c6409a0fca");
            this.DataChart1.Series.Add(lineSeries8);
            this.DataChart1.Size = new System.Drawing.Size(1060, 408);
            this.DataChart1.TabIndex = 0;
            this.DataChart1.Text = "ultraDataChart1";
            // 
            // tabPage1
            // 
            this.tabPage1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage1.BackgroundImage")));
            this.tabPage1.Controls.Add(this.buttonIzbrisiAgregator);
            this.tabPage1.Controls.Add(this.buttonIzbrisiUredjaj);
            this.tabPage1.Controls.Add(this.listBoxDevices);
            this.tabPage1.Controls.Add(this.buttonPrikaciNaAgregator);
            this.tabPage1.Controls.Add(this.buttonPrikaciNaSistem);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.DodajAgregator);
            this.tabPage1.Controls.Add(this.buttonUgasiAgregator);
            this.tabPage1.Controls.Add(this.buttonUpaliAgregator);
            this.tabPage1.Controls.Add(this.buttonUpaliDevice);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.listBoxAgregators);
            this.tabPage1.Controls.Add(this.buttonUgasiUredjaj);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1143, 594);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Kontrola uredjaja";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonIzbrisiAgregator
            // 
            this.buttonIzbrisiAgregator.Location = new System.Drawing.Point(758, 417);
            this.buttonIzbrisiAgregator.Name = "buttonIzbrisiAgregator";
            this.buttonIzbrisiAgregator.Size = new System.Drawing.Size(113, 50);
            this.buttonIzbrisiAgregator.TabIndex = 14;
            this.buttonIzbrisiAgregator.Text = "Izbrisi agregator";
            this.buttonIzbrisiAgregator.UseVisualStyleBackColor = true;
            this.buttonIzbrisiAgregator.Click += new System.EventHandler(this.buttonIzbrisiAgregator_Click);
            // 
            // buttonIzbrisiUredjaj
            // 
            this.buttonIzbrisiUredjaj.Location = new System.Drawing.Point(168, 419);
            this.buttonIzbrisiUredjaj.Name = "buttonIzbrisiUredjaj";
            this.buttonIzbrisiUredjaj.Size = new System.Drawing.Size(116, 48);
            this.buttonIzbrisiUredjaj.TabIndex = 13;
            this.buttonIzbrisiUredjaj.Text = "Izbrisi uredjaj";
            this.buttonIzbrisiUredjaj.UseVisualStyleBackColor = true;
            this.buttonIzbrisiUredjaj.Click += new System.EventHandler(this.buttonIzbrisiUredjaj_Click);
            // 
            // listBoxDevices
            // 
            this.listBoxDevices.FormattingEnabled = true;
            this.listBoxDevices.ItemHeight = 16;
            this.listBoxDevices.Location = new System.Drawing.Point(40, 88);
            this.listBoxDevices.Name = "listBoxDevices";
            this.listBoxDevices.Size = new System.Drawing.Size(245, 324);
            this.listBoxDevices.TabIndex = 0;
            // 
            // buttonPrikaciNaAgregator
            // 
            this.buttonPrikaciNaAgregator.Location = new System.Drawing.Point(291, 271);
            this.buttonPrikaciNaAgregator.Name = "buttonPrikaciNaAgregator";
            this.buttonPrikaciNaAgregator.Size = new System.Drawing.Size(120, 47);
            this.buttonPrikaciNaAgregator.TabIndex = 9;
            this.buttonPrikaciNaAgregator.Text = "Prikaci na agregator";
            this.buttonPrikaciNaAgregator.UseVisualStyleBackColor = true;
            this.buttonPrikaciNaAgregator.Click += new System.EventHandler(this.buttonPrikaciNaAgregator_Click);
            // 
            // buttonPrikaciNaSistem
            // 
            this.buttonPrikaciNaSistem.Location = new System.Drawing.Point(877, 271);
            this.buttonPrikaciNaSistem.Name = "buttonPrikaciNaSistem";
            this.buttonPrikaciNaSistem.Size = new System.Drawing.Size(113, 47);
            this.buttonPrikaciNaSistem.TabIndex = 12;
            this.buttonPrikaciNaSistem.Text = "Prikaci na sistem";
            this.buttonPrikaciNaSistem.UseVisualStyleBackColor = true;
            this.buttonPrikaciNaSistem.Click += new System.EventHandler(this.buttonPrikaciNaSistem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label1.Location = new System.Drawing.Point(98, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Uredjaji";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(39, 419);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 47);
            this.button1.TabIndex = 2;
            this.button1.Text = "Dodaj uredjaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.DodajUredjaj_Click);
            // 
            // DodajAgregator
            // 
            this.DodajAgregator.Location = new System.Drawing.Point(626, 419);
            this.DodajAgregator.Name = "DodajAgregator";
            this.DodajAgregator.Size = new System.Drawing.Size(113, 47);
            this.DodajAgregator.TabIndex = 5;
            this.DodajAgregator.Text = "Dodaj agregator";
            this.DodajAgregator.UseVisualStyleBackColor = true;
            this.DodajAgregator.Click += new System.EventHandler(this.DodajAgregator_Click);
            // 
            // buttonUgasiAgregator
            // 
            this.buttonUgasiAgregator.Location = new System.Drawing.Point(877, 150);
            this.buttonUgasiAgregator.Name = "buttonUgasiAgregator";
            this.buttonUgasiAgregator.Size = new System.Drawing.Size(113, 47);
            this.buttonUgasiAgregator.TabIndex = 11;
            this.buttonUgasiAgregator.Text = "Ugasi agregator";
            this.buttonUgasiAgregator.UseVisualStyleBackColor = true;
            this.buttonUgasiAgregator.Click += new System.EventHandler(this.buttonUgasiAgregator_Click);
            // 
            // buttonUpaliAgregator
            // 
            this.buttonUpaliAgregator.Location = new System.Drawing.Point(877, 88);
            this.buttonUpaliAgregator.Name = "buttonUpaliAgregator";
            this.buttonUpaliAgregator.Size = new System.Drawing.Size(113, 47);
            this.buttonUpaliAgregator.TabIndex = 10;
            this.buttonUpaliAgregator.Text = "Upali agregator";
            this.buttonUpaliAgregator.UseVisualStyleBackColor = true;
            this.buttonUpaliAgregator.Click += new System.EventHandler(this.buttonUpaliAgregator_Click);
            // 
            // buttonUpaliDevice
            // 
            this.buttonUpaliDevice.Location = new System.Drawing.Point(291, 88);
            this.buttonUpaliDevice.Name = "buttonUpaliDevice";
            this.buttonUpaliDevice.Size = new System.Drawing.Size(120, 47);
            this.buttonUpaliDevice.TabIndex = 7;
            this.buttonUpaliDevice.Text = "Upali uredjaj";
            this.buttonUpaliDevice.UseVisualStyleBackColor = true;
            this.buttonUpaliDevice.Click += new System.EventHandler(this.buttonUpaliDevice_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label2.Location = new System.Drawing.Point(675, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Agregatori";
            // 
            // listBoxAgregators
            // 
            this.listBoxAgregators.DisplayMember = "DeviceCode";
            this.listBoxAgregators.FormattingEnabled = true;
            this.listBoxAgregators.ItemHeight = 16;
            this.listBoxAgregators.Location = new System.Drawing.Point(626, 88);
            this.listBoxAgregators.Name = "listBoxAgregators";
            this.listBoxAgregators.Size = new System.Drawing.Size(245, 324);
            this.listBoxAgregators.TabIndex = 3;
            this.listBoxAgregators.ValueMember = "DeviceCode";
            // 
            // buttonUgasiUredjaj
            // 
            this.buttonUgasiUredjaj.Location = new System.Drawing.Point(291, 150);
            this.buttonUgasiUredjaj.Name = "buttonUgasiUredjaj";
            this.buttonUgasiUredjaj.Size = new System.Drawing.Size(120, 47);
            this.buttonUgasiUredjaj.TabIndex = 8;
            this.buttonUgasiUredjaj.Text = "Ugasi uredjaj";
            this.buttonUgasiUredjaj.UseVisualStyleBackColor = true;
            this.buttonUgasiUredjaj.Click += new System.EventHandler(this.buttonUgasiUredjaj_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(-2, -2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1151, 623);
            this.tabControl1.TabIndex = 13;
            // 
            // dBModelsTableAdapter
            // 
            this.dBModelsTableAdapter.ClearBeforeFill = true;
            // 
            // dBModelsTableAdapter1
            // 
            this.dBModelsTableAdapter1.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.OldLace;
            this.ClientSize = new System.Drawing.Size(1148, 621);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Advanced Metering Infrastructure";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dBModelsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemDatabaseDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart4)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dBModelsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemDatabaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart3)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataChart1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonIzlistaj;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dateTimePicker5DO;
        private System.Windows.Forms.DateTimePicker dateTimePicker5OD;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBoxMerenje5;
        private System.Windows.Forms.ComboBox comboBoxOperator;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button buttonIzvestaj4;
        private System.Windows.Forms.ComboBox comboBoxAgregator4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxMerenje4;
        private Infragistics.Win.DataVisualization.UltraDataChart DataChart4;
        private System.Windows.Forms.Button buttonIzvestaj3;
        private System.Windows.Forms.ComboBox comboBoxUredjaj3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label13;
        private Infragistics.Win.DataVisualization.UltraDataChart DataChart3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonIzvestaj2;
        private System.Windows.Forms.ComboBox comboBoxAgregator1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxMerenje2;
        private Infragistics.Win.DataVisualization.UltraDataChart DataChart2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonIzvestaj;
        private System.Windows.Forms.ComboBox comboBoxUredjaj1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxMerenje1;
        private Infragistics.Win.DataVisualization.UltraDataChart DataChart1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button buttonIzbrisiAgregator;
        private System.Windows.Forms.Button buttonIzbrisiUredjaj;
        private System.Windows.Forms.ListBox listBoxDevices;
        private System.Windows.Forms.Button buttonPrikaciNaAgregator;
        private System.Windows.Forms.Button buttonPrikaciNaSistem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button DodajAgregator;
        private System.Windows.Forms.Button buttonUgasiAgregator;
        private System.Windows.Forms.Button buttonUpaliAgregator;
        private System.Windows.Forms.Button buttonUpaliDevice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBoxAgregators;
        private System.Windows.Forms.Button buttonUgasiUredjaj;
        private System.Windows.Forms.TabControl tabControl1;
        private SystemDatabaseDataSet systemDatabaseDataSet;
        private System.Windows.Forms.BindingSource dBModelsBindingSource;
        private SystemDatabaseDataSetTableAdapters.DBModelsTableAdapter dBModelsTableAdapter;
        private SystemDatabaseDataSet1 systemDatabaseDataSet1;
        private System.Windows.Forms.BindingSource dBModelsBindingSource1;
        private SystemDatabaseDataSet1TableAdapters.DBModelsTableAdapter dBModelsTableAdapter1;
        private System.Windows.Forms.TabPage tabPage4;
    }
}

