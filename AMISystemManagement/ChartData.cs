﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AMISystemManagement
{
    public abstract class ObservableMode : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ProductionData : ObservableCollection<AMIDatabase.Model.DBModel>
    {

    }

    public class ChartData : ProductionData
    {
        public ChartData(List<AMIDatabase.Model.DBModel> database)
        {
            foreach (var item in database)
            {
                Add(item);
            }
        }
    }
}
