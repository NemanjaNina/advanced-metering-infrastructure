﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using AMIDatabase.Model;
using AMIDatabase.Access;

namespace AMISystemManagement
{
    public class SystemManagement : ISystemManagement
    {
        private DBModel modelZaBazu;    

        public SystemManagement()
        {
            modelZaBazu = new DBModel();      
        }

        public bool PrimiIzvestaj(PaketAgregatorSystem paket)
        {
            modelZaBazu.AgregatorCode = paket.AgregatorCode;
            modelZaBazu.AgregatorTimestamp = paket.Timestamp;
            List<Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>>> lista = paket.ListaMerenja.ToList();
            try
            {
                foreach (Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>> item in lista)
                {
                    SpremiModelZaUpis(item);
                    DBManager.Instance.AddDevice(modelZaBazu);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void SpremiModelZaUpis(Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>> paket)
        {
            modelZaBazu.DeviceCode = paket.Item1.Item1;
            modelZaBazu.DeviceTimestamp = paket.Item1.Item2;
            modelZaBazu.Napon = paket.Item2[0].Item2;
            modelZaBazu.Struja = paket.Item2[1].Item2;
            modelZaBazu.AktivnaSnaga = paket.Item2[2].Item2;
            modelZaBazu.ReaktivnaSnaga = paket.Item2[3].Item2;

        }
    }
}
