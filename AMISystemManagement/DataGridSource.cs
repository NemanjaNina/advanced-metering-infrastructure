﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMISystemManagement
{
        public class GridSource : ObservableCollection<AMIDatabase.Model.DBModel>
        {

        }
    public class DataGridSource : GridSource
    {
        public DataGridSource(List<AMIDatabase.Model.DBModel> database)
        {
            foreach (var item in database)
            {
                Add(item);
            }
        }
    }
}
