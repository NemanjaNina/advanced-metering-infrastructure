﻿using AMIDevice;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class AMIDeviceTest
    {
        Mock<Generator> generatorMock;
        [SetUp]
        public void SetUp()
        {
            generatorMock = new Mock<Generator>();
        }

        [Test]
        [TestCase(0.0, 0.0, ExpectedResult = 0)]
        [TestCase(17.0, 17.0, ExpectedResult = 17)]
        [TestCase(-7450.0, -7450.0, ExpectedResult = -7450)]
        public double GenerisiDouble_LosiParametri(double minimum, double maximum)
        {
            double rezultat = generatorMock.Object.GenerisiDouble(minimum, maximum);

            return rezultat;
        }
    }
}
