﻿using AMIAgregator;
using Common;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class AMIAgregatorTest
    {

       Mock<Agregator> mockAgregator;

       [SetUp]
       public void SetUp()
       {
           mockAgregator = new Mock<Agregator>();
       }

        [Test]
        [TestCase(null)]
        //[ExpectedException(typeof(ArgumentNullException))]
        public void PrimiIzvestaj_LosParametar(IDevice device)
        {
            mockAgregator.Object.PrimiIzvestaj(device);
        }
    }
}
