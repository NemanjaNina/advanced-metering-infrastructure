﻿using AMIDatabase.Access;
using AMIDatabase.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class AMIDataBaseTest
    {

        [Test]
        [TestCase(null)]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddDevice_LosParametar(DBModel newModel)
        {
            DBManager.Instance.AddDevice(newModel);
        }
    }
}
