﻿using AMIDatabase.Model;
using AMIDevice;
using AMISystemManagement;
using Common;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class AMISystemManagementTest
    {
        Mock<SystemManagement> mockSystemManagement;
        Mock<DBModel> mockModelZaBazu;
        Mock<List<IDevice>> mockUredjaji;

        [SetUp]
        public void SetUp()
        {
            mockSystemManagement = new Mock<SystemManagement>();
            mockModelZaBazu = new Mock<DBModel>();
            mockUredjaji = new Mock<List<IDevice>>();
        }

        [Test]
        [TestCase(null)]
        [ExpectedException(typeof(NullReferenceException))]
        public void PrimiIzvestaj_NullParametar(PaketAgregatorSystem paket)
        {
            mockSystemManagement.Object.PrimiIzvestaj(paket);
        }

        [Test]
        [TestCase(null)]
        [ExpectedException(typeof(NullReferenceException))]
        public void SpremiModelZaUpis_NullParametar(Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>> paket)
        {
            mockModelZaBazu.Object.DeviceCode = paket.Item1.Item1;
            mockModelZaBazu.Object.DeviceTimestamp = paket.Item1.Item2;
            mockModelZaBazu.Object.Napon = paket.Item2[0].Item2;
            mockModelZaBazu.Object.Struja = paket.Item2[1].Item2;
            mockModelZaBazu.Object.AktivnaSnaga = paket.Item2[2].Item2;
            mockModelZaBazu.Object.ReaktivnaSnaga = paket.Item2[3].Item2;

        }
        [Test]
        [TestCase(null)]
        [ExpectedException(typeof(NullReferenceException))]
        public void Reinicijalizuj_NullParametar(IDevice device)
        {
            device.Ugasi();
            mockUredjaji.Object.Remove(device);
            mockUredjaji.Object.Add(new Device());
            //PoveziListBoxSaDeviceListom();
        }
    }
}
