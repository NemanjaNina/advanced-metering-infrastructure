﻿using System;
using System.Collections.Generic;

namespace Common
{
    public class PaketAgregatorSystem
    {
        public List<Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>>> ListaMerenja { get; set; }
        public double Timestamp { get; set; }
        public int AgregatorCode { get; set; }
    }
}
