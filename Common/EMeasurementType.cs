﻿namespace Common
{
    public enum EMeasurementType
    {
        Napon,
        Struja,
        AktivnaSnaga,
        ReaktivnaSnaga
    }
}
