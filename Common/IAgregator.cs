﻿using System;
using System.Collections.Generic;

namespace Common
{
    public interface IAgregator
    {
        void PrimiIzvestaj(IDevice device);
        int AgregatorCode { get; }
        double Timestamp { get; }
        bool Upaljen { get; set; }
        string Upali();
        string Ugasi();
        ISystemManagement SystemManagement { get; set; }
        List<Tuple<Tuple<int, double>, List<Tuple<EMeasurementType, double>>>> BufferBaza { get; }
    }
}
