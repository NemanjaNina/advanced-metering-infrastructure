﻿using System;
using System.Collections.Generic;

namespace Common
{
    public interface IDevice
    {
        int DeviceCode { get; }
        bool Upaljen { get; set; }
        double Timestamp { get; }
        IAgregator Agregator { get; set; }
        IGenerator Generator { get; set; }
        List<Tuple<EMeasurementType, double>> ListaPodataka { get; }
        void GenerisiPodatke();
        string Upali();
        string Ugasi();
    }
}
